//
//  AppConfig.h
//  BONJOURproject
//
//  Created by Developer on 5/12/13.
//  Copyright (c) 2013 Developer. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface AppConfig : NSObject {
    NSString* name;
}

@property (retain) NSString* name;

// Singleton - one instance for the whole app
+ (AppConfig*)getInstance;

@end
