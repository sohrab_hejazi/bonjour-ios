//
//  ViewController.m
//  BONJOURproject
//
//  Created by Developer on 5/10/13.
//  Copyright (c) 2013 Developer. All rights reserved.
//
#import <netinet/in.h>
#import "ViewController.h"
#include <CFNetwork/CFSocketStream.h>

@interface ViewController ()

@end

@implementation ViewController
@synthesize userName;
@synthesize tbView;
@synthesize txView;
@synthesize sms;
@synthesize browser;
@synthesize members;

-(void)tableView:(UITextView *) tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //userName.text=[members objectAtIndex:indexPath.row];
}


- (void)netServiceBrowser:(NSNetServiceBrowser *)browser
           didFindService:(NSNetService *)aNetService
               moreComing:(BOOL)moreComing
{
    [self.members addObject:aNetService];
    NSLog(@"Got service %p with hostname %@\n", aNetService,
          [aNetService hostName]);
    
    txView.text = [txView.text stringByAppendingString:  @"service was found\n"];
 
    [self resolveIPAddress:aNetService];
   }

- (void)netServiceBrowser:(NSNetServiceBrowser *)browser
         didRemoveService:(NSNetService *)aNetService
               moreComing:(BOOL)moreComing
{
    [self.members removeObject:aNetService];
    
    txView.text = [txView.text stringByAppendingFormat:@"a member was removed \n"];
    if(!moreComing)
    {
        [self.tbView reloadData];
    }
}

-(void) browseServices {
    id delegateObject = nil; 
    //NSNetServiceBrowser *browser;
    
    browser = [[NSNetServiceBrowser alloc] init];
    [browser setDelegate:delegateObject];
    [browser searchForServicesOfType:@"_chatRoom._tcp." inDomain:@""];
    
  /*  services = [NSMutableArray new];
    self.browser = [[NSNetServiceBrowser new] autorelease];
    self.browser.delegate = self;
    [self.browser searchForServicesOfType:@"_MyService._tcp." inDomain:@""]; */
}


- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return [members count];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    //Region *region = [regions objectAtIndex:indexPath.section];
    //TimeZoneWrapper *timeZoneWrapper = [region.timeZoneWrappers objectAtIndex:indexPath.row];
    //cell.textLabel.text = timeZoneWrapper.localeName;
    cell.textLabel.text = [[members objectAtIndex:indexPath.row] hostName];
    return cell;
}

-(void)viewDidLoad {
    [self browseServices];
    [super viewDidLoad];
    members = [NSMutableArray new];
    self.browser = [[NSNetServiceBrowser new] autorelease];
    self.browser.delegate = self;
    [self.browser searchForServicesOfType:@"_chatRoom._tcp." inDomain:@""];
    
    userName.text=[[UIDevice currentDevice] name];
    //userName.text=[NSNetService hostName];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)userNameRegistering:(id)sender
{
    
    
     /*   id delegateObject = nil; // Assume this exists.
   // NSNetServiceBrowser *browser;
    
    browser = [[NSNetServiceBrowser alloc] init];
    [browser setDelegate:delegateObject];
    [browser searchForServicesOfType:@"_chatRoom._tcp." inDomain:@""]; */
}
-(IBAction)joinToGroup:(id)sender
{
    
}
-(IBAction)sendSms:(id)sender
{
    
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

////////////////////////////////////////////////////////////////////////resolve


-(void) resolveIPAddress:(NSNetService *)service {
    NSNetService *remoteService = service;
    remoteService.delegate = self;
    [remoteService resolveWithTimeout:0];
}

-(void)netServiceDidResolveAddress:(NSNetService *)service {
    NSString   *name =nil;
    NSData *address = nil;
    struct sockaddr_in *socketAddress = nil;
    NSString *ipString = nil;
    int port;
    
    for(int i=0;i < [[service addresses] count]; i++ )
    {
        //if(i==0) userName.text= [service name];
        name = [service name];
        address = [[service addresses] objectAtIndex: i];
        socketAddress = (struct sockaddr_in *) [address bytes];
        ipString = [NSString stringWithFormat: @"%s",inet_ntoa(socketAddress->sin_addr)];
        port = socketAddress->sin_port;
        txView.text = [txView.text stringByAppendingFormat:
                      @"Resolved: %@-->%@:%d\n", [service hostName], ipString, port];
    //////////////////////////////////////////////////////////////
    /*NSString *Fname=userName.text;
    Fname = [Fname stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSString *urlString = [NSString stringWithFormat:@"%@:%d?+%@+%@+",ipString, port, (Fname),sms.text];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    	NSURLConnection *conn = [NSURLConnection connectionWithRequest:request delegate:self];
    if (conn == nil) 
        NSLog(@"cannot create connection"); */
    
    }// end of for
    
    [self.tbView reloadData];
}

/*- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
   //Convert to string and append received data
    // NSUInteger  an_Integer;
    NSArray * ipItemsArray;
    //NSString *externalIP;
    
    
    NSString *theIpHtml = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
	//[webView loadHTMLString:theIpHtml baseURL:nil];
    NSScanner *theScanner;
    NSString *msg = [NSString stringWithFormat:@"message="];
    
    theScanner = [NSScanner scannerWithString:theIpHtml];
    
    ipItemsArray =[theIpHtml  componentsSeparatedByString:@"+"];
    //externalIP =[ipItemsArray objectAtIndex:  1];
    
    //message.text=[ipItemsArray objectAtIndex: 2 ];
    txView.text = [txView.text stringByAppendingFormat:
                   @"%@-->%@\n", [ipItemsArray objectAtIndex:  1], [ipItemsArray objectAtIndex: 2]]; 
}
*/

-(void)netService:(NSNetService *)service
    didNotResolve:(NSDictionary *)errorDict {
    txView.text = [txView.text stringByAppendingFormat:@"Could not resolve: %@\n", errorDict];
}
///////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////
- (void)dealloc {
    
    [browser release];
    [members release];
    [tbView release];
    [sms release];
    [txView release];
    [super dealloc];
}

@end
