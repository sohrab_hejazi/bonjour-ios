//
//  ConnectionDelegate.h
//  BONJOURproject
//
//  Created by Developer on 5/12/13.
//  Copyright (c) 2013 Developer. All rights reserved.
//
#import <Foundation/Foundation.h>

@class Connection;

@protocol ConnectionDelegate

- (void) connectionAttemptFailed:(Connection*)connection;
- (void) connectionTerminated:(Connection*)connection;
- (void) receivedNetworkPacket:(NSDictionary*)message viaConnection:(Connection*)connection;

@end
