//
//  ViewController.h
//  BONJOURproject
//
//  Created by Developer on 5/10/13.
//  Copyright (c) 2013 Developer. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ViewController : UIViewController{
    CFSocketRef listeningSocket;
}

@property (retain, nonatomic) IBOutlet UITextField *userName;
@property (retain, nonatomic) IBOutlet UITableView *tbView;
@property (retain, nonatomic) IBOutlet UITextView *txView;
@property (retain, nonatomic) IBOutlet UITextField *sms;
@property (readwrite, retain) NSNetServiceBrowser *browser;
@property (readwrite, retain) NSMutableArray *members;
-(IBAction) userNameRegistering: (id)sender;
-(IBAction)joinToGroup:(id)sender;
-(IBAction)sendSms:(id)sender;

@end
