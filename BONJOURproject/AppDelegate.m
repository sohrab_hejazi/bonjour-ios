//
//  AppDelegate.m
//  BONJOURproject
//
//  Created by Developer on 5/10/13.
//  Copyright (c) 2013 Developer. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"

@implementation AppDelegate

@synthesize window;
@synthesize viewController;
 NSNetService *service;
- (void)applicationDidFinishLaunching:(UIApplication *)application {
    
    
    [window addSubview:viewController.view];
   [window makeKeyAndVisible];
          
    
    service = [[NSNetService alloc] initWithDomain:@""// 1
                                              type:@"_chatRoom._tcp."
                                              name:@""
                                              port:7654];
    if(service)
    {
        service.delegate = self;
        [service publish];// 3
        NSLog(@"service publiched");    }
    else
    {
        NSLog(@"An error occurred initializing the NSNetService object.");
    }
}

-(void)service:(NSNetService *)aNetService
    didNotPublish:(NSDictionary *)dict {
    NSLog(@"Service did not publish: %@", dict);
}
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [service stop];}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
    [window addSubview:viewController.view];
    [window makeKeyAndVisible];
    
    
    service = [[NSNetService alloc] initWithDomain:@""// 1
                                              type:@"_chatRoom._tcp."
                                              name:@""
                                              port:7654];
    if(service)
    {
        service.delegate = self;
        [service publish];// 3
        NSLog(@"service publiched");    }
    else
    {
        NSLog(@"An error occurred initializing the NSNetService object.");
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [service stop];
}

- (void)dealloc {
    [service release];
    [viewController release];
    [window release];
    [super dealloc];
}


@end
